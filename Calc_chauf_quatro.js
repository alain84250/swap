// jshint esversion:6
(function () {"use strict";

window.onload = function() 
{
    function classEner()  // Je regarde quelle classe DPE a été sélectionnée.
    {
        let lettreClasse = ''; 
        for(let i = 0; i < document.getElementsByName("classe").length; i++)
        {
            if(document.getElementsByName("classe")[i].checked)
            {
                lettreClasse = document.getElementsByName("classe")[i].value;
            }
        }
        return lettreClasse;
    }

    function moyChauf()  // Je regarde la valeur du moyen de chauffage sélectionné.
    {
        let valeur = '';
        for(let i = 0; i < document.getElementsByName("moyen").length; i++)
        {
            if(document.getElementsByName("moyen")[i].checked)
            {
                valeur = document.getElementsByName("moyen")[i].value;
            }
        }
        return valeur;
    }

    function typeMoyChauf()  // Je regarde le type de moyen de chauffage sélectionné.
    {
        let genre = '';
        for(let i = 0; i < document.getElementsByName("moyen").length; i++)
        {
            if(document.getElementsByName("moyen")[i].checked)
            {
                genre = document.getElementsByName("moyen")[i].id;
            }
        }
        return genre;
    }

    function verifClasseDpe(lettreClasse)  // Je vérifie qu'une classe DPE à bien été sélectionnée.
    {
        let classeVerif = false;
        if(lettreClasse != '')
        {
            classeVerif = true;            
        }
        return classeVerif;
    }

    function verifValeurDpe(valeurExacteDPE)  //Je vérifie que le champ "Valeur DPE" est corectement rempli.
    {
        let valeurExacteDpeVerifiee = false;
        if(isNaN(valeurExacteDPE))
        {
            valeurExacteDpeVerifiee = false;
        }
        else if(valeurExacteDPE <= 0)
        {
            valeurExacteDpeVerifiee = false;
        }
        else
        {
            valeurExacteDpeVerifiee = true;
        }
        return valeurExacteDpeVerifiee;
    }

    function verifSurface(surface)  //Je vérifie que le champ "Surface" est corectement rempli.
    {
        let surfaceVerifiee = false;
        if(isNaN(surface))
        {
            surfaceVerifiee = false;
        }
        else if(surface <= 0)
        {
            surfaceVerifiee = false;
        }
        else
        {
            surfaceVerifiee = true;
        }
        return surfaceVerifiee;
    }

    function verifMoyChauf(valeur)  // Je vérifie qu'un moyen de chauffage à bien été sélectionné.
    {
        let valeurVerif = false;
        if(valeur != '')
        {
            valeurVerif = true;            
        }
        return valeurVerif;
    }

    function calculer()
    {
        let lettre = classEner();
        let coef1, coef2;
        let valeurExacteDPE = parseInt(document.getElementById('DPE').value, 10);
        let surface = parseInt(document.getElementById('surface').value, 10);
        let ratio = moyChauf();
        switch(lettre)
        {
            case "A":
                coef1 = 0;
                coef2 = 50;
                break;
            case "B":
                coef1 = 51;
                coef2 = 90;
                break;
            case "C":
                coef1 = 91;
                coef2 = 150;
                break;
            case "D":
                coef1 = 151;
                coef2 = 230;
                break;
            case "E":
                coef1 = 231;
                coef2 = 330;
                break;
            case "F":
                coef1 = 331;
                coef2 = 451;
                break;
            case "G":
                coef1 = 451;
                coef2 = 1000;
                break;
        }

        if(isNaN(valeurExacteDPE) == true)
        {
            let prixMin = (surface * coef1) * ratio;
            prixMin = Math.round(prixMin);
            document.getElementById('resultatMin').innerHTML = "Prix min: " + prixMin + " €";
            let prixMax = (surface * coef2) * ratio;
            prixMax = Math.round(prixMax);
            document.getElementById('resultatMax').innerHTML = "Prix max: " + prixMax + " € *";
            document.getElementById('classe_sélectionnée').innerHTML="Classe : " + classEner();
            document.getElementById('surface_sélectionnée').innerHTML="Surface : " + parseInt(document.getElementById('surface').value, 10);
            document.getElementById('chauffage_sélectionné').innerHTML="Pour un chauffage : " + typeMoyChauf();
        }
        if(lettre == '')
        {
            let prix = (surface * valeurExacteDPE) * ratio;
            prix = Math.round(prix);
            document.getElementById('resultat').innerHTML = "Prix : " + prix + " €";
            document.getElementById('valeur_classe').innerHTML="Valeur énergétique : " + parseInt(document.getElementById('DPE').value, 10);
            document.getElementById('surface_sélectionnée').innerHTML="Surface : " + parseInt(document.getElementById('surface').value, 10);
            document.getElementById('chauffage_sélectionné').innerHTML="Pour un chauffage : " + typeMoyChauf();
        }
    }

    function clearBtnRadio()
    {
        var ele = document.getElementsByName("classe");
        for(var i=0;i<ele.length;i++)
        {
            ele[i].checked = false;
        }
        return ele[i];
    }

    function clearValeurDpe()
    {
        var ele = document.getElementsByName("classe");
        var DPE = document.querySelector('#DPE');
        for(var i=0;i<ele.length;i++)
        {
            if(ele[i].checked == true)
            {
                DPE.value = '';
            }
        }
        return DPE.value;
    }

    function changeEtat()
    {
        let surface = parseInt(document.getElementById('surface').value, 10);
        let surfaceVerifiee = verifSurface(surface);
        let valeurExacteDPE = parseInt(document.getElementById('DPE').value, 10);
        let valeurDpeVerifiee = verifValeurDpe(valeurExacteDPE);
        let ratio = moyChauf();
        let valeurVerif = verifMoyChauf(ratio);
        let lettre = classEner();
        let lettreClasseVerif = verifClasseDpe(lettre);
        if ((surfaceVerifiee == true) && (valeurVerif == true) && ((valeurDpeVerifiee == true) || (lettreClasseVerif == true)))
        {
            if((document.getElementById('pas_cliquable')!= null) && (document.getElementById('pas_cliquable').value !='')){
                document.getElementById('pas_cliquable').id="generer";
                document.getElementById('generer').addEventListener("click", moncalCulerFlip, false );
            }
            if((document.getElementById('generer')!= null) && (document.getElementById('generer').value !='')){
                document.getElementById('generer').innerHTML="Calculer";
            }
        }
    }

    function flip()
    {
        var card = document.querySelector('.card');
        card.classList.toggle('is-flipped');
    }

    var moncalCulerFlip =  function () 
    {
        calculer();
        flip();
    };

    document.querySelector('.boutons_dpe').addEventListener ("click", function() {clearValeurDpe(), changeEtat();});
    document.querySelector('#DPE').addEventListener ("keydown", function() {clearBtnRadio(), changeEtat();});
    document.querySelector('#surface').addEventListener("keydown", function() {changeEtat();});
    document.querySelector('.bouton_chauffage').addEventListener ("click", function() {changeEtat();});

    document.querySelector('.refaire_calcul').addEventListener("click", function() 
    {	
		document.getElementById('generer').removeEventListener("click", moncalCulerFlip );
        document.getElementById("calculatrice").reset();
        document.querySelector('.card').classList.toggle('is-flipped');
        document.getElementById('generer').id="pas_cliquable";
        document.getElementById('pas_cliquable').innerHTML="Veuillez remplir tous les champs";
        document.getElementById('resultatMin').innerHTML = "";
		document.getElementById('resultatMax').innerHTML = "";
		document.getElementById('classe_sélectionnée').innerHTML="";
		document.getElementById('surface_sélectionnée').innerHTML="";
		document.getElementById('chauffage_sélectionné').innerHTML="";
		document.getElementById('resultat').innerHTML = "";
		document.getElementById('valeur_classe').innerHTML="";
    });
};
}());
